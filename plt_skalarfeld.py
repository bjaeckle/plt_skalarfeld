"""
Plote ein Skalarfeld. Die Levels können als Argument --levels=100 oder --level 100
übergeben werden.
Copyright Beat Jäckle, 2024
"""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axisartist.axislines import AxesZero

# https://matplotlib.org/stable/gallery/axisartist/demo_axisline_style.html
import sys

LEVELS: int = 100

args: list = []
for arg in sys.argv[1:]:
    args += arg.split('=')

i: int = 0
while i < len(args):
    arg: str = args[i]
    if arg == '--levels':
        i += 1
        LEVELS = int(args[i])
    else:
        print('WARNING: Unknown arg', arg)

    i += 1
del i
del args
# sys.exit(1)

fig = plt.figure()
ax = fig.add_subplot(axes_class=AxesZero)

for direction in ["xzero", "yzero"]:
    # adds arrows at the ends of each axis
    ax.axis[direction].set_axisline_style("-|>")

    # adds X and Y-axis from the origin
    ax.axis[direction].set_visible(True)

for direction in ["left", "right", "bottom", "top"]:
    # hides borders
    ax.axis[direction].set_visible(False)

# https://numpy.org/doc/stable/reference/generated/numpy.meshgrid.html#numpy.meshgrid
n = [1,2]
x = np.linspace(-10, 10, 101)
y = np.linspace(-10, 10, 101)

xs, ys = np.meshgrid(x, y, sparse=True)
z = n[0]*xs + n[1]*ys
h = plt.contourf(x, y, z,
                 levels=LEVELS,
                 )
plt.axis('scaled')
plt.colorbar()
# plt.axline((0, 0), slope=0, color='k')
# plt.axline((0, 10), slope='inf', color='k')
for direction in ["xzero", "yzero"]:
    ax.axis[direction].set_axisline_style("-|>")
    ax.axis[direction].set_visible(True)
plt.savefig(f'test1_{LEVELS}.pdf')
# plt.show()
