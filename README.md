# Plotte normales Skalarfeld

Dieses Python Skript plottet ein Skalarfeld.
Das Skalar von einem Punkt entspricht dem Skalarprodukt von dem Normalvektor und dem Ortsvektor des Punktes.

Copyright: Beat Jäckle, 2024
